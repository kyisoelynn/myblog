<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'PostController@index');
Route::get('/upload', 'PostController@create');
Route::post('/add', 'PostController@store');
Route::get('post/{post}', 'PostController@show');
Route::get('/post/delete/{post}', 'PostController@destroy');
Route::get('/post/edit/{post}', 'PostController@edit');
Route::post('/post/edit', 'PostController@update');

Route::post('/comment', 'CommentController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/categories', 'CategoryController@index');
Route::get('/add_category', 'CategoryController@create');
Route::post('/save_category', 'CategoryController@store');
Route::get('/category/edit/{cat_id}', 'CategoryController@edit');
Route::get('/category/delete/{cat_id}', 'CategoryController@destroy');


//For Admin Table Auth

Route::get('admin-login', 'Auth\AdminLoginController@showLoginForm');
Route::post('admin-login', 'Auth\AdminLoginController@login');

Route::get('/backend', 'AdminController@index');