@extends('layouts.template')

@section('content')

<div class="col-md-8">
	<h2>Edit Category</h2>

	<form action="/update_category" method="post">
		@csrf
		<input type="hidden" name="cat_id" value="{{ $cat_id->id }}">
		<div class="form-group">
			<label>Category Name</label>
			<input type="text" name="name" class="form-control" value="{{ $cat_id->category_name }}">
		</div>
		<div class="form-group">
			<input type="submit" name="update" value="Update" class="btn btn-primary">
		</div>
	</form>
</div>

@endsection