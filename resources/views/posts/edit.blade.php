@extends('layouts.template')

@section('content')

@if(count($errors))
	<div class="alert alert-danger">
		<ul>
			@foreach($errors as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="col-md-8">
	<form method="post" action="/post/edit" enctype="multipart/form-data" class="my-3">
		@csrf
		<input type="hidden" name="post_id" value="{{ $post->id }}">
		<div class="form-group">
			<label>Post Title</label>
			<input type="text" name="title" class="form-control" value="{{ $post->title }}">
		</div>

		<div class="form-group">
			<label>Category</label>
			<select name="category" class="form-control">
				@foreach($categories as $category)
				<option value="{{ $category->id }}" 
					@if($category->id == $post->category_id) {{'selected' }}
					@endif
					>
					{{ $category->category_name }}</option>
					
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Photo</label>
			<input type="file" name="photo" class="form-control-file">
			<input type="hidden" name="image" value="{{ $post->photo }}">

		</div>

		<div class="form-group">
			<label>Post Body</label>
			<textarea id="summernote" class="form-control" name="body">{{ $post->body }}</textarea>
		</div>

		<div class="form-group">
			<input type="submit" name="update" value="Update" class="btn btn-primary">
		</div>
	</form>
</div>

@endsection