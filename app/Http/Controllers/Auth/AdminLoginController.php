<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminLoginController extends Controller
{
    //

    public function showLoginForm(){
    	return view('auth.adminLogin');
    }

    public function login(Request $request) {
    	//Validate
    	$this->validate($request, [
    		'email' => 'required| email',
    		'password' => 'required|min:6'
    	]);

		if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
		    		//if success
			return redirect()->intended('backend');
		}

		return back()->withErrors(['email' => 'Email or password are wrong.']);
    }
}
